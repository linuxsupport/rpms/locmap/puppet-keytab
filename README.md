# Deprecated

As of October 2023, the functionality that this module provided has now been integrated into the [puppet-kerberos](https://gitlab.cern.ch/linuxsupport/rpms/locmap/puppet-kerberos) module.