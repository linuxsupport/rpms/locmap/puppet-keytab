Name:           puppet-keytab
Version:        2.0
Release:        5%{?dist}
Summary:        Puppet module for keytab

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Puppet module needed for kerberos module.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/keytab/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/keytab/
touch %{buildroot}/%{_datadir}/puppet/modules/keytab/supporting_module

%files -n puppet-keytab
%{_datadir}/puppet/modules/keytab
%doc code/README.md

%changelog
* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.0-5
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0-4
- fix requires on puppet-agent

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-3
- Rebuild for el8

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-2
- Execute cern-get-keytab only on the cern.ch domain

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebuild for 7.5 release

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Wed Jun 15 2016 Aris Boutselis <aris.boutselis@cern.ch>
-Initial release
