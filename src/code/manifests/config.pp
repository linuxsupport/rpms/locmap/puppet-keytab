class keytab::config {

  if $facts['domain'] == 'cern.ch' {
    exec {'cern-config-keytab':
      command => '/usr/sbin/cern-get-keytab',
      creates => '/etc/krb5.keytab',
      require => [Package['cern-get-keytab'],
      ],
    }
  }
}
